/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controle;

import Modelo.Paciente;
import Modelo.Secretaria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MatheusLeite
 */
public class ControleSecretaria {
    public int validaSecretaria;
    private ArrayList<String> listaConsultas;
    public static int contaSecretarias = 0;

    public int getContaSecretarias() {
        return contaSecretarias;
    }

    public void setContaSecretarias(int contaSecretarias) {
        this.contaSecretarias = contaSecretarias;
    }
    
    public String marcarConsulta(Paciente umPaciente, String data){
        umPaciente.setData(data);
        listaConsultas.add(data);
        return null;
    }
    
    public String desmarcarConsulta(Paciente umPaciente){
        umPaciente.setData("À definir");
        return null;
    }
    
    public static void adicionarSecretaria(Secretaria secretaria){
        String sql = "INSERT INTO listasecretarias(nome, cpf, login, senha, horarioAtendimento, agenda)" + " VALUES(?,?,?,?,?,?)";
        
        Connection conexao = null;
        PreparedStatement pstm = null;
        
        try{
            conexao = GeradorConexao.abrirConexao();
            pstm = conexao.prepareStatement(sql);
            
            pstm.setString(1,secretaria.getNome());
            pstm.setString(2,secretaria.getCpf());
            pstm.setString(3, secretaria.getLogin());
            pstm.setString(4,secretaria.getSenha());
            pstm.setString(5,secretaria.getHorarioAtendimento());
            pstm.setString(6, secretaria.getAgenda());
            pstm.execute();
        } catch (Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(pstm != null){
                    pstm.close();
                }
                if(conexao != null){
                    conexao.close();
                }
                
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    
    public static void removerSecretaria(String nome){
        String sql = "DELETE FROM listasecretarias WHERE nome = ?";
        Connection conn = null;
        PreparedStatement pstm = null;
 
        try{
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, nome);
 
            pstm.execute();
 
            } catch (Exception e) {
                e.printStackTrace();
            }finally{
                try{
                    if(pstm != null){
                        pstm.close();
                    }
                    if(conn != null){
                        conn.close();
                    }
                }catch(Exception e){
            e.printStackTrace();
                }
            }
    
    }
    
    public static void atualizarSecretaria(Secretaria secretaria){
        String sql = "UPDATE listasecretarias SET nome = ?, login = ?, senha = ?, cpf = ?, horarioAtendimento =?, agenda = ?" +
        " WHERE nome = ?";
 
        Connection conn = null;
        PreparedStatement pstm = null;
 
        try {
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            
            pstm.setString(1,secretaria.getNome());
            pstm.setString(2,secretaria.getCpf());
            pstm.setString(3, secretaria.getLogin());
            pstm.setString(4,secretaria.getSenha());
            pstm.setString(5,secretaria.getHorarioAtendimento());
            pstm.setString(6, secretaria.getAgenda());
            pstm.setString(7,secretaria.getNome());
            
            pstm.execute();
 
            } catch (Exception e) {
                e.printStackTrace();
                }finally{
         try{
            if(pstm != null){
                pstm.close();
            }
 
            if(conn != null){
                conn.close();
            }
            }catch(Exception e){
                e.printStackTrace();
            }
            }
}
    
    public static List<Secretaria> getSecretarias(){
        String sql = "SELECT * FROM listasecretarias";
        
        List<Secretaria> secretarias = new ArrayList<Secretaria>();
        
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rset = null;
        
        try {
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            rset = pstm.executeQuery();
            
            while(rset.next()){
                Secretaria secretaria = new Secretaria("-", "-");
                
                secretaria.setNome(rset.getString("Nome"));
                secretaria.setCpf(rset.getString("CPF"));
                secretaria.setLogin(rset.getString("Login"));
                secretaria.setSenha(rset.getString("Senha"));
                secretaria.setHorarioAtendimento(rset.getString("horarioAtendimento"));
                secretaria.setAgenda(rset.getString("Agenda"));
                secretarias.add(secretaria);
                contaSecretarias = contaSecretarias + 1;
            }
            
            } catch(Exception e){
                e.printStackTrace();
            } finally{
            try{
                if(rset != null){
                    rset.close();
                }
                if(pstm != null){
                    pstm.close();
                }
                if(conn != null){
                    conn.close();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        }
        return secretarias;
    }
    
    
public Secretaria validaLoginSecretaria(String usuario, String senha){
   
       for(Secretaria secretaria : getSecretarias()){
           if(usuario.equals(secretaria.getLogin())){
               if(senha.equals(secretaria.getSenha())){
                    validaSecretaria = 1;
               }
           }else{
               validaSecretaria = 0;
           }
       }
        return null;
    }
        
}

    
    

