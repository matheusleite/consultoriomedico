/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import Modelo.Paciente;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MatheusLeite
 */
public class ControlePaciente {
    
    private static int contaPacientes=0;

    public static int getcontaPacientes() {
        return contaPacientes;
    }

    public static void setcontaPacientes(int contaPacientes) {
        ControlePaciente.contaPacientes = contaPacientes;
    }
    //adiciona um novo paciente na tabela 'listapacientes' do banco de dados
    public static void adicionarPaciente(Paciente paciente){
        String sql = "INSERT INTO listapacientes(nome, login, senha, idade, cpf, telefone, sexo, tipoSanguíneo, fumante, alergia, peso, altura, imc,diagnostico, dataConsulta, cidade)" + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        Connection conexao = null;
        PreparedStatement pstm = null;
        
        try{
            conexao = GeradorConexao.abrirConexao();
            pstm = conexao.prepareStatement(sql);
            
            pstm.setString(1,paciente.getNome());
            pstm.setString(2, paciente.getLogin());
            pstm.setString(3,paciente.getSenha());
            pstm.setInt(4, paciente.getIdade());
            pstm.setString(5,paciente.getCpf());
            pstm.setInt(6, paciente.getTelefone());
            pstm.setString(7,paciente.getSexo());
            pstm.setString(8, paciente.getTipoSanguineo());
            pstm.setString(9,paciente.getFumante());
            pstm.setString(10, paciente.getAlergia());
            pstm.setString(11,paciente.getPeso());
            pstm.setString(12, paciente.getAltura());
            pstm.setString(13,paciente.getImc());
            pstm.setString(14, paciente.getDiagnóstico());
            pstm.setString(15,paciente.getData());
            pstm.setString(16, paciente.getCidade());
            pstm.execute();
        } catch (Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(pstm != null){
                    pstm.close();
                }
                if(conexao != null){
                    conexao.close();
                }
                
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    //remove um paciente da tabela do banco de dados
    public static void removerPaciente(String nome){
        String sql = "DELETE FROM listapacientes WHERE nome = ?";
        Connection conn = null;
        PreparedStatement pstm = null;
 
        try{
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, nome);
 
            pstm.execute();
 
            } catch (Exception e) {
                e.printStackTrace();
            }finally{
                try{
                    if(pstm != null){
                        pstm.close();
                    }
                    if(conn != null){
                        conn.close();
                    }
                }catch(Exception e){
            e.printStackTrace();
                }
            }
 }
    //atualiza os dados de um paciente na tabela do banco de dados
    public static void atualizarPaciente(Paciente paciente){
        String sql = "UPDATE listapacientes SET nome = ?, login = ?, senha = ?, idade = ?, cpf = ?, telefone = ?, sexo = ?, tipoSanguíneo = ?, fumante = ?, alergia = ?, peso =?, altura =?, imc = ?, diagnostico = ?, dataConsulta=?, cidade = ?" +
        " WHERE nome = ?";
 
        Connection conn = null;
        PreparedStatement pstm = null;
 
        try {
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);

            pstm.setString(1,paciente.getNome());
            pstm.setString(2, paciente.getLogin());
            pstm.setString(3,paciente.getSenha());
            pstm.setInt(4, paciente.getIdade());
            pstm.setString(5,paciente.getCpf());
            pstm.setInt(6, paciente.getTelefone());
            pstm.setString(7,paciente.getSexo());
            pstm.setString(8, paciente.getTipoSanguineo());
            pstm.setString(9,paciente.getFumante());
            pstm.setString(10, paciente.getAlergia());
            pstm.setString(11,paciente.getPeso());
            pstm.setString(12, paciente.getAltura());
            pstm.setString(13,paciente.getImc());
            pstm.setString(14, paciente.getDiagnóstico());
            pstm.setString(15,paciente.getData());
            pstm.setString(16, paciente.getCidade());
            pstm.setString(17,paciente.getNome());
            pstm.execute();
 
            } catch (Exception e) {
                e.printStackTrace();
                }finally{
         try{
            if(pstm != null){
                pstm.close();
            }
 
            if(conn != null){
                conn.close();
            }
            }catch(Exception e){
                e.printStackTrace();
            }
            }
}
    //lista todos os pacientes cadastrados no banco de dados
    public static List<Paciente> getPacientes(){
        String sql = "SELECT * FROM listapacientes";
        
        List<Paciente> pacientes = new ArrayList<Paciente>();
        
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rset = null;
        
        try {
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            rset = pstm.executeQuery();
            
            while(rset.next()){
                Paciente paciente = new Paciente("-", "-");
                
                paciente.setNome(rset.getString("Nome"));
                paciente.setLogin(rset.getString("Login"));
                paciente.setSenha(rset.getString("Senha"));
                paciente.setIdade(rset.getInt("Idade"));
                paciente.setCpf(rset.getString("CPF"));
                paciente.setTelefone(rset.getInt("Telefone"));
                paciente.setSexo(rset.getString("Sexo"));
                paciente.setTipoSanguineo(rset.getString("tipoSanguíneo"));
                paciente.setFumante(rset.getString("Fumante"));
                paciente.setAlergia(rset.getString("Alergia"));
                paciente.setPeso(rset.getString("Peso"));
                paciente.setAltura(rset.getString("Altura"));
                paciente.setImc(rset.getString("imc"));
                paciente.setData(rset.getString("dataConsulta"));
                paciente.setDiagnóstico(rset.getString("diagnostico"));
                paciente.setCidade(rset.getString("Cidade"));
                pacientes.add(paciente);
                contaPacientes = contaPacientes + 1;
                
            }
            
            } catch(Exception e){
                e.printStackTrace();
            } finally{
            try{
                if(rset != null){
                    rset.close();
                }
                if(pstm != null){
                    pstm.close();
                }
                if(conn != null){
                    conn.close();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        }
        return pacientes;
    }
}
