/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controle;

import Modelo.Paciente;
import java.util.ArrayList;

/**
 *
 * IMPORTANTE!! A lista de pacientes foi criada aqui e não na classe ControlePaciente porque
 * tanto o médico como a secretaria precisam ter acesso a listaPacientes
 */
public class ControleConsultorio {
    
    public ArrayList<Paciente> listaPacientes;

    public ArrayList<Paciente> getListaPacientes() {
        return listaPacientes;
    }

    public void setListaPacientes(ArrayList<Paciente> listaPacientes) {
        this.listaPacientes = listaPacientes;
    }
    
    public ControleConsultorio(){
        
        listaPacientes = new ArrayList<Paciente>();
    }
    
    public String adicionar(Paciente paciente) {
        String mensagem = "Paciente adicionado com Sucesso!";
		listaPacientes.add(paciente);
		return mensagem;
    }

    public String remover(Paciente paciente) {
        String mensagem = "Paciente removido com Sucesso!";
        listaPacientes.remove(paciente);
        return mensagem;
    }
    
    public Paciente pesquisarNome(String umNome) {
        for (Paciente paciente: listaPacientes) {
            if (paciente.getNome().equalsIgnoreCase(umNome)) return paciente;
        }
        return null;
    }
}
