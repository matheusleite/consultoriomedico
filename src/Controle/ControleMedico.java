/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controle;

import Modelo.Medico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class ControleMedico {
    
    
    public int valida;
    public static int contaMedicos = 0;

    public static int getContaMedicos() {
        return contaMedicos;
    }

    public static void setContaMedicos(int contaMedicos) {
        ControleMedico.contaMedicos = contaMedicos;
    }
    
    public static void adicionarMedico(Medico medico){
        String sql = "INSERT INTO listamedicos(nome, login, senha, cpf,especialidade, formacao, crm)" + " VALUES(?,?,?,?,?,?,?)";
        
        Connection conexao = null;
        PreparedStatement pstm = null;
        
        try{
            conexao = GeradorConexao.abrirConexao();
            pstm = conexao.prepareStatement(sql);
            
            pstm.setString(1,medico.getNome());
            pstm.setString(2, medico.getLogin());
            pstm.setString(3,medico.getSenha());
            pstm.setString(4,medico.getCpf());
            pstm.setString(5,medico.getEspecialidade());
            pstm.setString(6, medico.getFormacao());
            pstm.setString(7,medico.getCrm());
            pstm.execute();
        } catch (Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(pstm != null){
                    pstm.close();
                }
                if(conexao != null){
                    conexao.close();
                }
                
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    
    public static void removerMedico(String nome){
        String sql = "DELETE FROM listamedicos WHERE nome = ?";
        Connection conn = null;
        PreparedStatement pstm = null;
 
        try{
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            pstm.setString(1, nome);
 
            pstm.execute();
 
            } catch (Exception e) {
                e.printStackTrace();
            }finally{
                try{
                    if(pstm != null){
                        pstm.close();
                    }
                    if(conn != null){
                        conn.close();
                    }
                }catch(Exception e){
            e.printStackTrace();
                }
            }
    
    }
    
    public static void atualizarMedico(Medico medico){
        String sql = "UPDATE listamedicos SET nome = ?, login = ?, senha = ?, cpf = ?, especialidade =?, formacao = ?, crm = ?" +
        " WHERE nome = ?";
 
        Connection conn = null;
        PreparedStatement pstm = null;
 
        try {
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            
            pstm.setString(1,medico.getNome());
            pstm.setString(2, medico.getLogin());
            pstm.setString(3,medico.getSenha());
            pstm.setString(4,medico.getCpf());
            pstm.setString(5,medico.getEspecialidade());
            pstm.setString(6, medico.getFormacao());
            pstm.setString(7,medico.getCrm());
            
            pstm.execute();
 
            } catch (Exception e) {
                e.printStackTrace();
                }finally{
         try{
            if(pstm != null){
                pstm.close();
            }
 
            if(conn != null){
                conn.close();
            }
            }catch(Exception e){
                e.printStackTrace();
            }
            }
}
    
    public static List<Medico> getMedicos(){
        String sql = "SELECT * FROM listamedicos";
        
        List<Medico> medicos = new ArrayList<Medico>();
        
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rset = null;
        
        try {
            conn = GeradorConexao.abrirConexao();
            pstm = conn.prepareStatement(sql);
            rset = pstm.executeQuery();
            
            while(rset.next()){
                Medico medico = new Medico("-", "-");
                
                medico.setNome(rset.getString("Nome"));
                medico.setLogin(rset.getString("Login"));
                medico.setSenha(rset.getString("Senha"));
                medico.setCpf(rset.getString("CPF"));
                medico.setEspecialidade(rset.getString("especialidade"));
                medico.setFormacao(rset.getString("Formacao"));
                medico.setCrm(rset.getString("crm"));
                medicos.add(medico);
                contaMedicos = contaMedicos + 1;
            }
            
            } catch(Exception e){
                e.printStackTrace();
            } finally{
            try{
                if(rset != null){
                    rset.close();
                }
                if(pstm != null){
                    pstm.close();
                }
                if(conn != null){
                    conn.close();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        }
        return medicos;
    }
    
    public Medico validaLogin(String usuario, String senha){
   
       for(Medico medico : getMedicos()){
           if(usuario.equals(medico.getLogin())){
               if(senha.equals(medico.getSenha())){
                 valida = 1;
               }
           }else{
               valida = 0;
           }
       }
        return null;
    }
    
    public static void main(String[] args){
        removerMedico("Edgar Ramos");
    }
}
